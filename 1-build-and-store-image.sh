#!/bin/bash

set -e

source common.sh

REPO_NAME=`generate_name`
echo "using repository name $REPO_NAME"
ACCT_ID=`aws sts get-caller-identity --query "Account" --output text`
REGION=`aws configure get region`
IMAGE_NAME="$ACCT_ID.dkr.ecr.$REGION.amazonaws.com/$REPO_NAME:latest"

# Create new ECR repository
aws ecr create-repository --repository-name $REPO_NAME
aws ecr get-login-password --region $REGION | docker login --username AWS --password-stdin $ACCT_ID.dkr.ecr.$REGION.amazonaws.com


# Build image and push it to it
docker buildx build --platform linux/amd64 --tag $IMAGE_NAME --push -f container-image/Dockerfile .


cleanup "aws ecr batch-delete-image  --repository-name $REPO_NAME --image-ids imageTag=latest"
cleanup "aws ecr delete-repository --repository-name $REPO_NAME"

ROLE_ARN="arn:aws:iam::$ACCT_ID:role/ecsTaskExecutionRole"

# Generate task definition, which references the region and image
cat << EOF > task-definition.json
{

    "executionRoleArn": "$ROLE_ARN",
    "requiresCompatibilities": [
        "FARGATE"
    ],
    "inferenceAccelerators": [],
    "containerDefinitions": [
        {
            "name": "ci-coordinator",
            "image": "$IMAGE_NAME",
            "essential": true,
            "portMappings": [
                {
                    "containerPort": 22,
                    "protocol": "tcp"
                }
            ],
            "entryPoint": [],
            "command": [],
            "environmentFiles": [],
            "logConfiguration": {
                "logDriver": "awslogs",
                "options": {
                    "awslogs-group": "/ecs/test-task",
                    "awslogs-region": "$REGION",
                    "awslogs-stream-prefix": "ecs"
                }
            }
           
        }
    ],
    "volumes": [],
    "networkMode": "awsvpc",
    "memory": "2048",
    "cpu": "1024",

    "family": "test-task",
    "runtimePlatform": {
        "operatingSystemFamily": "LINUX"
    }
}
EOF
