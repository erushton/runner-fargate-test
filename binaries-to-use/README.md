
The `get-latest-runner.sh` script is a helper to just grab the latest published one and put it in the right place. If you're using a custom build just copy it to this directory and name it `gitlab-runner` manually.

You can probably guess, but the same goes for the Fargate driver binary. Use the `get-latest-fargate-driver.sh` to grab the latest official one, or if you're using a custom build copy it to this directory.

In both cases make sure the binary is compiled for `amd64` - which if you're on a Mac M1/M2 is not the default target.