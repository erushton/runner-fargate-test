#!/bin/bash

set -e

source common.sh

# Check that AWS CLI is available and configured
aws ec2 describe-instances > /dev/null

# Check docker / buildx
docker buildx version

# Is docker running?
docker info

# Log default region because this is the easiest way to think you've made a mistake
echo AWS region in use is `aws configure get region`

# Check we have a dict file for generating random name
wc -l /usr/share/dict/words

# Check we have jq
jq --version

within_vpc_quota

# All good
echo
echo All good.
