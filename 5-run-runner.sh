#!/bin/bash

source vars.sh

ssh ubuntu@$IP_ADDR -i $KEY_FILE "sudo /home/ubuntu/gitlab-runner run"