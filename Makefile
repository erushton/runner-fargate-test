sane:
	./0-sanity.sh

BIN_PATH=binaries-to-use

$(BIN_PATH)/gitlab-runner:
	curl -o $(BIN_PATH)/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

$(BIN_PATH)/fargate:
	curl -o $(BIN_PATH)/fargate https://gitlab-runner-custom-fargate-downloads.s3.amazonaws.com/latest/fargate-linux-amd64

build-and-push: $(BIN_PATH)/gitlab-runner
	./1-build-and-store-image.sh

launch:
	./2-launch-instance.sh

install-runner: $(BIN_PATH)/fargate $(BIN_PATH)/gitlab-runner
	./3-install-runner.sh

create-cluster:
	./4-create-cluster.sh

run-runner:
	./5-run-runner.sh

all: cleanup sane build-and-push launch install-runner create-cluster run-runner

cleanup:
	./cleanup.sh

clean:
	rm -f cleanup.sh vars.sh key.json
