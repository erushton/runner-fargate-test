#!/bin/bash

# https://docs.gitlab.com/runner/configuration/runner_autoscale_aws_fargate/#step-3-create-an-ec2-instance-for-gitlab-runner

set -e
source common.sh

require_ami_id_set

#### VPC
within_vpc_quota

VPC_ID=`aws ec2 create-vpc --cidr-block 10.0.0.0/23 | jq -r ".Vpc.VpcId"`

INET_GW_ID=`aws ec2 create-internet-gateway | jq -r ".InternetGateway.InternetGatewayId"`

aws ec2 attach-internet-gateway --vpc-id $VPC_ID --internet-gateway-id $INET_GW_ID
RTB_ID=`aws ec2 describe-route-tables --filters Name=vpc-id,Values=$VPC_ID | jq -r ".RouteTables[0].RouteTableId"`
aws ec2 create-route --route-table-id $RTB_ID --destination-cidr-block 0.0.0.0/0 --gateway-id $INET_GW_ID

echo "🤖 Created VPC"

#### Subnet
SUBNET_ID=`aws ec2 create-subnet --vpc-id $VPC_ID --cidr-block 10.0.0.0/24 | jq -r ".Subnet.SubnetId"`
echo "🤖 Created subnet"

#### Security Group
SG_ID=`aws ec2 create-security-group --description fargate-test --group-name fargate-test --vpc-id $VPC_ID | jq -r ".GroupId"`
aws ec2 authorize-security-group-ingress --group-id $SG_ID --protocol tcp --port 22 --cidr 0.0.0.0/0
echo "🤖 Created security group"

#### IAM Role
IAM_NAME=fargate-`generate_name`
aws iam create-role --role-name $IAM_NAME --assume-role-policy-document file://policy.json
cleanup "aws iam delete-role --role-name $IAM_NAME"

PROFILE_NAME=profile-`generate_name`
aws iam create-instance-profile --instance-profile-name $PROFILE_NAME
aws iam add-role-to-instance-profile --instance-profile-name $PROFILE_NAME --role-name $IAM_NAME
cleanup "aws iam delete-instance-profile --instance-profile-name $PROFILE_NAME"
 aws iam attach-role-policy --policy-arn arn:aws:iam::aws:policy/AmazonECS_FullAccess --role-name $IAM_NAME

echo "🤖 Created IAM role $IAM_NAME and instance profile $PROFILE_NAME"

#### Key pairs
KEY_NAME=fargate-`generate_name`
aws ec2 create-key-pair --key-name $KEY_NAME > key.json
cat key.json
cleanup "rm key.json"
cleanup "aws ec2 delete-key-pair --key-name $KEY_NAME"

KP_ID=`jq -r ".KeyPairId" key.json`
KEY_FILE=key-`generate_name`
jq -r ".KeyMaterial" key.json > $KEY_FILE
chmod 400 $KEY_FILE

cleanup "chmod 600 $KEY_FILE"
cleanup "rm -f $KEY_FILE"

echo "🤖 Created key-pair $KEY_NAME"

###
echo "SUBNET_ID=$SUBNET_ID" >> vars.sh
echo "SG_ID=$SG_ID" >> vars.sh
echo "KEY_FILE=$KEY_FILE" >> vars.sh
#### EC2 Instance
echo "🤖 Launching instance"
echo "Logging vars"
cat vars.sh

sleep 10
RUN_INSTANCE_CMD="aws ec2 run-instances --image-id $AMI_ID --count 1 --instance-type t2.micro  --iam-instance-profile Name=$PROFILE_NAME --security-group-ids $SG_ID --subnet-id $SUBNET_ID --associate-public-ip-address --key-name $KEY_NAME"
echo "🤖 Attempting to launch instance with"
echo "   $RUN_INSTANCE_CMD"
INSTANCE_ID=`$RUN_INSTANCE_CMD| jq -r ".Instances[0].InstanceId"`

cleanup "aws ec2 terminate-instances --instance-ids $INSTANCE_ID"

# Clean up of VPC will fail while instance is alive
cleanup "aws ec2 wait instance-terminated --instance-ids $INSTANCE_ID"
cleanup "aws ec2 delete-subnet --subnet-id $SUBNET_ID"
cleanup "aws ec2 detach-internet-gateway --internet-gateway-id $INET_GW_ID --vpc-id $VPC_ID"
cleanup "aws ec2 delete-security-group --group-name $SG_ID"
cleanup "aws ec2 delete-internet-gateway --internet-gateway-id $INET_GW_ID"
cleanup "aws ec2 delete-vpc --vpc-id $VPC_ID"

# Waaaaiiiiit for it
aws ec2 wait instance-running --instance-ids $INSTANCE_ID
aws ec2 wait instance-status-ok --instance-ids $INSTANCE_ID

IP_ADDR=`aws ec2 describe-instances --instance-ids $INSTANCE_ID | jq -r ".Reservations[0].Instances[0].PublicIpAddress"`

echo "IP_ADDR=$IP_ADDR" >> vars.sh



echo "Instance launched. You should be able to SSH with the following command"
echo "    ssh ubuntu@$IP_ADDR -i $KEY_FILE"
