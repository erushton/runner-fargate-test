# Goal

The goal of this project is to have a "one click" gitlab-runner + fargate environment that lets us test out new Fargate driver changes (or new runner changes with Fargate)

This is mostly accomplisehd via the AWS CLI and using cloud sandbox AWS account.

The main setup instructions for Runner + Fargate are found at https://docs.gitlab.com/runner/configuration/runner_autoscale_aws_fargate/

# Usage

The main interface should be the `Makefile`. However you could run the step scripts in order - they are named with the order they are run in which sfollows the above mentioned setup instructions for Runner + Fargate (the numbers don't quite line up, but the order is the same)

Not implemented yet, but eventually there will be one `make` target which lets you go from zero to a fully operational Fargate runner complete with custom docker image and provisioned infra.

## Steps

### 0 Sanity

This script checks that any prerequisite software or configuration is in place

### 1 Build and Store Image

This script builds the custom image (which needs to contain a runner binary), creates an ECR registry, and pushes the images to it.

See the `container-image/` directory for details on the container image, what it contains, and how to change which runner it includes.

### 2 Launch Instance

This script launches an EC2 instance which will serve as the "Runner Manager", with all the requisite supporting infra (IAM, Security Group, VPC, etc). At the end of the (successful) run you are able to SSH into the instance with a newly generated key.

### 3 Install Runner

This script is not yet complete. The purpose of this script is to install the runner and the Fargate driver on the EC2 instance and configure them via their `toml` files. The runner also needs to be registered with a gitlab project in this script.

### 4 Create Cluster

This script is also not yet complete, but is close. The purpose of this script is to create the ECS/Fargate cluster and task definition.

After this step you should have a Fargate runner connected to a project ready to pick up jobs. Try it?

### Clean Up

Each script appends to `cleanup.sh` any commands needed to tear down any infra the script creates. Running `make clean` will execute the clean up script, and then overwrite it with an empty version so it's ready for next time.

