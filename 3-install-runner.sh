#!/bin/bash

# https://docs.gitlab.com/runner/configuration/runner_autoscale_aws_fargate/#step-4-install-and-configure-gitlab-runner-on-the-ec2-instance

set -e
source common.sh

require_reg_token_set

source vars.sh

echo "🤖 Creating dirctories on runner manager instance"
ssh ubuntu@$IP_ADDR -i $KEY_FILE "sudo mkdir -p /opt/gitlab-runner/{metadata,builds,cache}"

echo "🤖 Copying runner binary to runner manager instance"
scp -i $KEY_FILE ./binaries-to-use/gitlab-runner ubuntu@$IP_ADDR:/home/ubuntu/

cat > config-templ.toml << EOF
[[runners]]
  builds_dir = "/opt/gitlab-runner/builds"
  cache_dir = "/opt/gitlab-runner/cache"
  [runners.custom]
    config_exec = "/opt/gitlab-runner/fargate"
    config_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "config"]
    prepare_exec = "/opt/gitlab-runner/fargate"
    prepare_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "prepare"]
    run_exec = "/opt/gitlab-runner/fargate"
    run_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "run"]
    cleanup_exec = "/opt/gitlab-runner/fargate"
    cleanup_args = ["--config", "/etc/gitlab-runner/fargate.toml", "custom", "cleanup"]
EOF

echo "🤖 Copying runner binary to runner manager instance"
scp -i $KEY_FILE config-templ.toml ubuntu@$IP_ADDR:/home/ubuntu/
rm config-templ.toml

echo "🤖 Registering runner"
ssh ubuntu@$IP_ADDR -i $KEY_FILE  "chmod +x /home/ubuntu/gitlab-runner; sudo /home/ubuntu/gitlab-runner register --url https://gitlab.com/ --registration-token $REG_TOKEN --name fargate-test-runner --run-untagged --executor custom -n --template-config config-templ.toml"

# At this point runner is registered and configured, but not running. 
# Fargate driver is not installed nor configured.

REGION=`aws configure get region`
CLUSTER_NAME=fargate-`generate_name`
cat > fargate.toml << EOF
LogLevel = "info"
LogFormat = "text"

[Fargate]
  Cluster = "$CLUSTER_NAME"
  Region = "$REGION"
  Subnet = "$SUBNET_ID"
  SecurityGroup = "$SG_ID"
  TaskDefinition = "test-task:1"
  EnablePublicIP = true

[TaskMetadata]
  Directory = "/opt/gitlab-runner/metadata"

[SSH]
  Username = "root"
  Port = 22
EOF

echo "CLUSTER_NAME=$CLUSTER_NAME" >> vars.sh

echo "🤖 Copying Fargate config to runner manager instance"
scp -i $KEY_FILE fargate.toml ubuntu@$IP_ADDR:/home/ubuntu/
rm fargate.toml

echo "🤖 Moving Fargate config to correct location"
ssh ubuntu@$IP_ADDR -i $KEY_FILE "sudo mv /home/ubuntu/fargate.toml /etc/gitlab-runner/fargate.toml"

echo "🤖 Copying Fargate binary to runner manager instance"
scp -i $KEY_FILE binaries-to-use/fargate ubuntu@$IP_ADDR:/home/ubuntu/
echo "🤖 Moving Fargate binary and setting execute"
ssh ubuntu@$IP_ADDR -i $KEY_FILE "sudo mv /home/ubuntu/fargate /opt/gitlab-runner/fargate ; sudo chmod +x /opt/gitlab-runner/fargate"

echo "🤖 Fargate driver is now installed and configured! 🎉"
