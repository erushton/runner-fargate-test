#
# Common functions used in multiple scripts
#

function cleanup() {
	echo $1 >> cleanup.sh
}

function generate_name() {
	# TODO - not sure if this will work on none macOS
	shuf -n 3 /usr/share/dict/words | tr '\n' '-' | tr '[:upper:]' '[:lower:]' | sed "s/\-*$/\n/ ; s/'//g"
}

function within_vpc_quota() {
	if [ `aws ec2 describe-vpcs | jq '.[] | length'` -ge 5 ]; then 
		echo "You don't have room in your AWS account to make a new VPC"
		echo "Script bailing out now to save you problems"
		exit 1
	fi
}

function require_reg_token_set() {
	if [ -z "$REG_TOKEN" ]; then 
		echo "Env variable REG_TOKEN is blank or unset";
		echo "Set the REG_TOKEN to the value of the registration token for your GitLab project"
		exit 1
	fi
}

function require_ami_id_set() {
	if [ -z "$AMI_ID" ]; then 
		echo "Env variable AMI_ID is blank or unset";
		echo "Set the AMI_ID to a valid image ami for your region $REGION"
		exit 1
	fi
}
