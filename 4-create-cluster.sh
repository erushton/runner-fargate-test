#!/bin/bash

set -e
source common.sh
source vars.sh

# https://docs.gitlab.com/runner/configuration/runner_autoscale_aws_fargate/#step-5-create-an-ecs-fargate-cluster



aws ecs create-cluster --cluster-name $CLUSTER_NAME --capacity-providers FARGATE FARGATE_SPOT --default-capacity-provider-strategy capacityProvider=FARGATE,weight=1,base=1
cleanup "aws ecs delete-cluster --cluster-name $CLUSTER_NAME"

aws ecs register-task-definition --cli-input-json file://task-definition.json
cleanup "aws ecs deregister-task-definition --task-definition test-task:1"